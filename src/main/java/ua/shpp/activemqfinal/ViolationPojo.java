package ua.shpp.activemqfinal;

import jakarta.validation.ConstraintViolation;
import jakarta.validation.Validation;
import jakarta.validation.Validator;
import jakarta.validation.ValidatorFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Set;

public class ViolationPojo {

    private final Logger log = LoggerFactory.getLogger(ViolationPojo.class);

    public Set<ConstraintViolation<Pojo>> getViolation(Pojo pojo) {
        ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory();
        Validator validator = validatorFactory.getValidator();
        return validator.validate(pojo);
    }
}
