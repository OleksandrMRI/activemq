package ua.shpp.activemqfinal;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

public class CheckIs7 implements ConstraintValidator<Check, Integer> {
    Integer val;
    public void initialize(Check count) {
        val = count.value();
    }

    @Override
    public boolean isValid(Integer integer, ConstraintValidatorContext constraintValidatorContext) {
        return integer%val!=0;
    }
}
