package ua.shpp.activemqfinal;

import jakarta.validation.ConstraintViolation;
import org.apache.commons.csv.CSVPrinter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.ObjectMessage;
import java.io.IOException;
import java.util.Set;

public class Consumer {
    private final FilePrinter invalidFilePrinter;
    private final FilePrinter validFilePrinter;
    private final MessageConsumer messageConsumer;

    private final Logger log = LoggerFactory.getLogger(Consumer.class);

    public Consumer(FilePrinter invalid, FilePrinter valid, MessageConsumer consumer) {
        this.invalidFilePrinter = invalid;
        this.validFilePrinter = valid;
        this.messageConsumer = consumer;
    }

    public void consumeMessages(String poison) throws JMSException, IOException {
        int counter = 0;
        ViolationPojo violationPojo = new ViolationPojo();
        while (true) {
            Message message = messageConsumer.receive(1000);
            if (message != null) {
                ObjectMessage objectMessage = (ObjectMessage) message;

                Pojo pojo = (Pojo) objectMessage.getObject();

                Set<ConstraintViolation<Pojo>> violations = violationPojo.getViolation(pojo);
                if (violations.isEmpty()) {
                    validFilePrinter.printPojo(pojo);
                } else {
                    invalidFilePrinter.printPojo(pojo, violations);
                }
                if (poison!=null&&pojo.getName().equals(poison)) {
                    log.info("Catch poison");
                    break;
                }
            } else {
                break;
            }
        }
    }
}
