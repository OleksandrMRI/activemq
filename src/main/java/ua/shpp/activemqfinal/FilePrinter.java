package ua.shpp.activemqfinal;

import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.validation.ConstraintViolation;
import org.apache.commons.csv.CSVPrinter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class FilePrinter {
    private final Logger log = LoggerFactory.getLogger(FilePrinter.class);
    private final String OUTSIDE_PROPERTIES = "conf/config.properties";

    CSVPrinter printer;

    public FilePrinter(CSVPrinter printer) {
        this.printer = printer;
    }

    public void printPojo(Pojo pojo) {
        printPojo(pojo, Collections.emptySet());
    }

    public void printPojo(Pojo pojo, Set<ConstraintViolation<Pojo>> violations) {
        try {
            String name = pojo.getName();
            int count = pojo.getCount();
            int counter = 0;
            if (violations.isEmpty()) {
                printer.printRecord(name, count + "\n");
                log.info("{}, {}, {}", counter++, name, count);
            } else {
                ErrorPojo errors = new ErrorPojo().setErrors(violations.stream().map(v -> v.getMessage()).collect(Collectors.toList()));
                ObjectMapper objectMapper = new ObjectMapper();
                String error = objectMapper.writeValueAsString(errors);
                printer.printRecord(name, count, error + "\n");
                log.info("{}, {}, {}, {}", counter++, name, count, error);
            }
        } catch (IOException e) {
            log.error("IOException, file to print is not exist: ", e);
        }
    }
}
