package ua.shpp.activemqfinal;

import java.util.Random;

public class Name {
    private int LENGTH = 15;
    private int CHAR_A = 65;
    private int CHAR_a = 97;
    private int ALPHABET_LENGTH = 25;
    StringBuilder sb = new StringBuilder();

    public String createName(Random random) {
        int length = random.nextInt(LENGTH);

        for (int i = 0; i <= length; i++) {
            int caseType = random.nextBoolean() ? CHAR_A : CHAR_a;
            sb.append((char) (caseType + random.nextInt(ALPHABET_LENGTH)));
        }
        return sb.toString();
    }
}
