package ua.shpp.activemqfinal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.Properties;

public class PropertiesReader {
    private final String INSIDE_PROPERTIES = "conf/config.properties";
    private Properties properties;
    private final String outside_properties;
    private Logger log = LoggerFactory.getLogger(PropertiesReader.class);

    public PropertiesReader(Properties properties, String outside_properties) {
        this.properties = properties;
        this.outside_properties = outside_properties;
    }

    String readProperty(String property) {
        if (new File(outside_properties).exists()) {
            extractProperties(outside_properties);
        } else {
            extractProperties(INSIDE_PROPERTIES);
        }
        return properties.getProperty(property);
    }

    private Properties extractProperties(String pathToFileConfig) {
        try (InputStream is = new FileInputStream(pathToFileConfig);
             InputStreamReader isr = new InputStreamReader(is, StandardCharsets.UTF_8);
             BufferedReader br = new BufferedReader(isr)) {
            properties.load(br);
        } catch (IOException e) {
            log.error("FileNotFoundException: ", e);
        }
        return properties;
    }
}
