package ua.shpp.activemqfinal;

import org.junit.jupiter.api.Test;

import java.util.Properties;

import static org.junit.jupiter.api.Assertions.*;

class PropertiesReaderTest {

    @Test
    void readProperty() {
        Properties properties = new Properties();
        PropertiesReader pr = new PropertiesReader(properties,"conf/config.property");
        assertEquals("3",pr.readProperty("time"));
    }
}