package ua.shpp.activemqfinal;

import org.junit.jupiter.api.Test;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Random;

import static org.junit.jupiter.api.Assertions.*;

class NameTest {

    @Test
    void createNameCheckIsResultStringLengthMinValue() throws NoSuchAlgorithmException {
        Random random = SecureRandom.getInstanceStrong();
        Name name = new Name();
        assertTrue(name.createName(random).length() > 0);
    }

    @Test
    void createNameCheckIsResultStringLengthMaxValue() throws NoSuchAlgorithmException {
        Random random = SecureRandom.getInstanceStrong();
        Name name = new Name();
        assertTrue(name.createName(random).length() <= 15);
    }
}